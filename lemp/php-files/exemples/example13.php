<?php

declare(strict_types=1); // strict requirements
function add($x1,$x2) : float  //con el : float indico que devuelve un float
{
    $y = $x1 + $x2;
    return $y;
}

echo "Add: " . add(5,6) . "<br>";

function add2($x1,float $x2=10) : float  //con el : float indico que devuelve un float
{
    $y = $x1 + $x2;
    return $y;
}

echo "Add2: " . add2(5) . "<br>";

?>