<?php

    $array = array("RED", "BLUE", "GREEN");

    $array2 = ["CYAN", "YELLOW", "MAGENTA", "BLACK"];

    echo $array[0] . "<br />";
    echo $array[1] . "<br />";
    echo $array[2] . "<br />";


    $len= count($array);

    for ($i=0; $i< count($array); $i++){
        echo $array[$i] . " ";
    }

    $games = array(
        "Playstation 2" => "RED",
        "Game Boy" => "Green",
        "Wii" => "Blue",
    ) ;
    echo "<br />";
    echo $games["Game Boy"] . "<br />";
    foreach ($games as $key => $value){
        echo $key . ":" . $value . "<br />";
    }

    //multi
    
    echo "<br />";
    echo "<br />";
    echo "<br />";
    echo "<h1>Ejemplo Array de dos</h1>";
$cars = array (
  array("Volvo",22,18),
  array("BMW",15,13),
  array("Saab",5,2),
  array("Land Rover",17,15)
);
echo $cars[0][0] . ": In stock: " . $cars[0][1] . ", sold: " . $cars[0][2] . ".<br>";
echo $cars[1][0] . ": In stock: " . $cars[1][1] . ", sold: " . $cars[1][2] . ".<br>";
echo $cars[2][0] . ": In stock: " . $cars[2][1] . ", sold: " . $cars[2][2] . ".<br>";
echo $cars[3][0] . ": In stock: " . $cars[3][1] . ", sold: " . $cars[3][2] . ".<br>";
    
echo "<br />";
echo "<br />";
echo "<br />";
echo "<h1>Ejemplo Array con listas</h1>";

for ($row = 0; $row < 4; $row++) {
    echo "<p><b>Row number $row</b></p>";
    echo "<ul>";
    for ($col = 0; $col < 3; $col++) {
        echo "<li>" . $cars[$row][$col] . "</li>";
    }
    echo "</ul>";
}
?>