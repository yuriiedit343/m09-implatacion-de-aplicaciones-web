<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $h1 = "Title Page";
        $txt= "<h1>" . $h1 . "</h1>";
        $txt .="<h2>Header h2</h2>";
        echo $txt;

        $p = "This is a very silly line with a \$txt";
        echo "<p>$p</p>";
        function f1(){
            //global $div hace que la variable se guarde fuera de la funcion
            $div = "This is a div";
            echo "<div>$div</div>";
        }

        f1();

        echo $GLOBALS['h1']; // la varaible globals tiene todas las variables del fichero y puedes consultar


        function f2(){
            static $i= 0; //static hace que cuando la funcion acabe se guarda el resultado
            echo $i;
            $i++;
        }

        echo "<br />";
        f2() . "<br />"; 
        f2() . "<br />";
        f2() . "<br />";
        echo "<br />";

        if(isset($i)){   //demuestra si la varaible existe o no
            echo "la variable \$i existeix" . "<br />"; 
        } else{
            echo "la variable \$i no existeix :(" . "<br />";
        }
        
        $y=100;
        $f = 11.4;
        $b = "text";
        $be = True;

        $result = sprintf("this is the result:  %d, %f, %s ,%b", $y, $f,$b,$be);
        echo " <br />" . $result . "<br />";

        $lorem = "mucoooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo testo y mas a mas mas mas mas mas mas mas ";
        echo strlen($lorem) . "<br />"; // cuantas letras tienes
        echo str_word_count($lorem) . "<br />"; // cuantas palabras tiene
        echo strpos($lorem, "testo") . "<br />"; // en que letra se encuentra lo que buscas


        $z = 120;
        echo PHP_INT_MAX . "<br />";

        echo $z * 2.54 . "<br />";

        echo is_int($z) . "<br />"; //true o false segun si es integer o no


        echo var_dump($z) . "<br />"; // te dice que tipo de variable es y lo que vale

        echo intval($z) . "<br />"; // convierte en integer

        if (!is_nan($z)){ //isnan te dice si es un numero o no
            echo $z+1 . "<br />";
            echo var_dump($z+1) . "<br />";
        }
        $x = 5985;
        $y = 10.25;
        echo "is_numeric($x): " . is_numeric($x) . "<br>";
        var_dump(is_numeric($x));
        echo "is_numeric($y): " . is_numeric($y) . "<br>";
        var_dump(is_numeric($y));
        
        
        $x = "5985";
        $y = 100;
        echo "$x + $y = " . $x + $y . "<br>";
        
        $x = "59.85";
        $y = 1000;
        echo "$x + $y = " . $x + $y . "<br>";
        
        $x = "Hello";
        var_dump(is_numeric($x));

        $a= 10;
        $b = 4;
        $pow= $a ** $b;
        echo "pow = $pow" . "<br />";

        $res= $a <=> $b;
        echo $res . "<br />"; // te dise quien es mas xiquito

        $c= 120;
        $d = 40;

        $res2 = $a == $b ? ($c > $d ? "1" : "2") : "3"; // si 
        echo $res . "<br />";
    ?>

</body>
</html>