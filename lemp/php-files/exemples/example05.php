<?php
define("GREETING", "Hello!");
define("languages", [
    "C++",
    "PHP",
    "Java"
]);

echo GREETING . "<br />";
echo languages[0] . ", " . languages[1] . ", " . languages[2] . "<br />";
echo "<br />";

function myTest()
{
    echo GREETING . '<br />';
    echo languages[0] . ", " . languages[1] . ", " . languages[2];
}

myTest();
?>