<?php

$dispositius = array (
    array("Playstation 2",1555),
    array("Nintendo DS",154),
    array("Game Boy",119),
    array("Play Station 4",102),
    array("Wii",101),
    array("Play Station 3",87),
    array("Xbox 360",84),
    array("Play Station Portable",82),
    array("Game Boy Advance",81),
    array("Nintendo 3DS",72),
    array("Nes",62),
    array("Nintendo Switch",60),
);

// Función de comparación para ordenar por ventas
function compararPorVentas($a, $b) {
    // Comparamos los números de ventas
    // Si $a es mayor que $b, devuelve un número negativo
    // Si $a es menor que $b, devuelve un número positivo
    // Si $a es igual que $b, devuelve 0
    return $b[1] - $a[1]; // Orden descendente
}

// Ordenar el arreglo por número de ventas
usort($dispositius, 'compararPorVentas');

// Encontrar el número máximo de ventas
$maxVentas = $dispositius[0][1];

// Crear la tabla
echo '<table>';
echo '<tr><th>Consola</th><th>Vendes</th><th>Barra</th></tr>';

foreach ($dispositius as $dispositiu) {
    echo '<tr>';
    echo '<td>' . $dispositiu[0] . '</td>'; // Nombre de la consola
    echo '<td>' . $dispositiu[1] . '</td>'; // Número de ventas

    echo '<td class="barra">'; // Barra de ventas 
    // Generar una barra visual basada en el número de ventas
    $barraLength = ($dispositiu[1] * 100) / $maxVentas; // Calcula la longitud de la barra relativa al máximo
    for ($j = 0; $j < $barraLength; $j++) {
        echo '<img src="green.png" alt="green">';
    }
    echo '</td>';
    echo '</tr>';
}

echo '</table>';
?>
