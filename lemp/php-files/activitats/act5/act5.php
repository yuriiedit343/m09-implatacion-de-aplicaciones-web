<?php
    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["execute"])) {
        execute();
    }
    function validate($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    function execute()
    {
        if (isset($_POST["price"]) && isset($_POST["tax"])) {
            $price = $_POST["price"];
            $tax_rate = $_POST["tax"];
    
            // Validar que ambos valores sean números
            if (is_numeric($price) && is_numeric($tax_rate)) {
                $price = floatval($price);
                $tax_rate = floatval($tax_rate);
    
                // Validar que la tasa de impuestos esté en el rango de 0 a 100
                if ($tax_rate >= 0 && $tax_rate <= 100) {
                    $tax_amount = $price * ($tax_rate / (100 + $tax_rate));
                    
                    echo "Price without tax: " . $price - $tax_amount . "<br />";
                    echo "Price without tax using round: " . round($price - $tax_amount,2) . "<br />";
                    echo "Price without tax using sprintf: " . sprintf("%.4f", $price - $tax_amount) . "<br />";
                    echo "Tax amount: " . $tax_amount . "<br />";
                } else {
                    echo "Invalid tax rate. Please enter a number between 0 and 100.";
                }
            } else {
                echo "Invalid input. Please enter numeric values only.";
            }
        } else {
            echo "Please fill in all fields.";
        }
    }
    
    ?>