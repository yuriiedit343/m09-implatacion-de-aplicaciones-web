<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulari</title>
    <style>
        .required {
            color: red;
        }
    </style>
</head>
<body>
    <h1>PRICE, TAX and ROUNDS</h1>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
        <label for="price">Price with TAX:</label><br />
        <input id="price" type="text" name="price" required value="<?php echo isset($_POST['price']) ? htmlspecialchars($_POST['price']) : '';?>" /><span class="required">*</span><br /><br />
        <label for="tax">TAX(%):</label><br />
        <input id="tax" type="text" name="tax" required value="<?php echo isset($_POST['tax'])?  htmlspecialchars($_POST['tax']) : ''?>"><span class="required">*</span><br /><br />
        <input type="submit" name="execute">
    </form>
    <h2>TAX DATA:</h2>
    <?php include 'act5.php';?>
</body>
</html>