<?php
$daus = array (
    array("images/1.png", 1),
    array("images/2.png", 2),
    array("images/3.png", 3),
    array("images/4.png", 4),
    array("images/5.png", 5),
    array("images/6.png", 6)
);

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $number = $_POST['number'];
    if (is_numeric($number) && $number > 0) {
        $sum = 0;
        echo "<h2>Resultats dels Daus:</h2>";
        echo "<p>";
        for ($i = 0; $i < $number; $i++) {
            $roll = rand(0, 5); // El indice es de 0 a 5
            $image = $daus[$roll][0];
            $dice_value = $daus[$roll][1];
            echo "<img src='$image' alt='Dau $dice_value'>";
            $sum += $dice_value;
        }
        echo "</p>";
        echo "<h2>Suma Total: $sum</h2>";
    } else {
        echo "<p style='color: red;'>Si us plau, introdueix un nombre vàlid de daus.</p>";
    }
}
?>
