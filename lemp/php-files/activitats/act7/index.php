<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exponent Calculator</title>
    <!-- Bootstrap CSS -->  <link rel="stylesheet" href="act7.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <!-- Tu CSS personalizado -->
  
</head>
<body>
<?php include 'act7.php'; ?>
<div class="mt-4">
    <h1>Exponent Calculator</h1>
</div>
<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <label for="base">Base:</label><br>
    <input type="text" id="base" name="base" value="<?php echo isset($_POST['base']) ? $_POST['base'] : ''; ?>" required><br>
    <label for="exponent">Exponent:</label><br>
    <input type="text" id="exponent" name="exponent" value="<?php echo isset($_POST['exponent']) ? $_POST['exponent'] : ''; ?>" required><br>
    <button type="submit">Calcular Potència</button>
    <div class="mt-4">
        <?php  echo numcalculat(); ?>
    </div>

</form><br>
<div class="container mt-2">
    <h2>Exponentes Positivos</h2>
    <p>La exponenciación es una operación matemática, escrita como \(x^n\), que implica la base \(x\) y un exponente \(n\). En el caso en que \(n\) sea un número entero positivo, la exponenciación corresponde a la multiplicación repetida de la base, \(n\) veces.</p>
    <p>La calculadora acepta bases negativas, pero no calcula números imaginarios. Tampoco acepta fracciones, pero puede usarse para calcular exponentes fraccionarios, siempre y cuando los exponentes se introduzcan en su forma decimal.</p>

    <h2>Exponentes Negativos</h2>
    <p>Cuando un exponente es negativo, el signo negativo se elimina reciprocando la base y elevándola al exponente positivo.</p>

    <h2>Exponente 0</h2>
    <p>Cuando un exponente es 0, cualquier número elevado a la potencia 0 es 1. Para \(0\) elevado a la potencia \(0\), la respuesta es \(1\), sin embargo, esto se considera una definición y no un cálculo real.</p>

    <h2>Exponente Número Real</h2>
    <p>Para números reales, utilizamos la función PHP <code>pow(n,x)</code>.</p>
</div>