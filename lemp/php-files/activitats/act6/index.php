<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Factorial Calculator</title>
    <!-- Bootstrap CSS -->  <link rel="stylesheet" href="act6.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <!-- Tu CSS personalizado -->
  
</head>

<body>

    <?php include 'act6.php'; ?>
    <div class="container">
        <h1 class="mt-5">Factorial Calculator</h1>
        <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" class="mt-3">
            <div class="form-group">
                <label for="number">Enter a number:</label>
                <input type="number" name="number" id="number" class="form-control" required value="<?php echo isset($_POST["number"]) ? $_POST["number"] : ''; ?>">
            </div>
            <button type="submit" class="btn btn-primary">Calculate Factorial</button>
            <div class="mt-4">
                <?php echo nomstrarnum(); ?>
            </div>
        </form>

        <h2 class="mt-5">Factorial Formulas</h2>
        <p>The formula to calculate a factorial for a number is:
            n! = n × (n-1) × (n-2) × ... × 1
            Thus, the factorial n! is equal to n times n minus 1, times n minus 2, continuing until the
            number 1 is reached.
            The factorial of zero is 1:
            0! = 1
        </p>
        <h2>Recurrence Relation</h2>
        <p>
            And the formula expressed using the recurrence relation looks like this:
            n! = n × (n – 1)!
            So the factorial n! is equal to the number n times the factorial of n minus one. This recurses
            until n is equal to 0.
        </p>
        <p>
            The table below shows the factorial n! for the numbers one to one-hundred.
        </p>
        <?php echo showFactorialTable(); ?>
    </div>



    <!-- Bootstrap JS (opcional, solo si necesitas funcionalidades de Bootstrap JS) -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    
</body>

</html>