<?php
// Función para validar datos
function validate($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

// Función para calcular el factorial de un número
function factorial($n) {
    if ($n === 0) {
        return 1;
    } else {
        return $n * factorial($n - 1);
    }
}

// Función para mostrar el resultado del factorial de un número
function showFactorialResult($number) {
    if ($number >= 0 && $number <= 100) {
        echo "<h2>Factorial of $number:</h2>";
        echo "<p>$number! = " . factorial($number) . "</p>";
    } else {
        echo "<p>El numero ha de ser entre 0 i 100.</p>";
    }
}

// Función para mostrar la tabla de factoriales
function showFactorialTable() {
    echo "<h2 class='pepe'>Factorial Table</h2>";
    echo "<table border='1'>";
    echo "<tr><th>Number</th><th>Factorial</th></tr>";
    for ($i = 0; $i <= 100; $i++) {
        echo "<tr>";
        echo "<td>$i</td>";
        $factous = factorial($i);
        echo "<td>$factous</td>";
        echo "</tr>";
    }
    echo "</table>";
}

// Lógica para procesar el formulario si se ha enviado
function nomstrarnum() {
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $number = validate($_POST["number"]);
    showFactorialResult($number);
}
}
?>
