<!DOCTYPE html>
<html lang="ca">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="act2.css">
    <title>Resultats del sinus i cosinus</title>
</head>
<body>
    <div class="container mt-5">
        <h1 class="text-center">Resultats del sinus i cosinus</h1>
        <img src="cos.png" class="img-fluid mx-auto d-block mb-5" alt="Imatge que vulguis">
        <div class="table-responsive">
            <table class="table table-bordered mx-auto">
                <thead class="thead-dark">
                    <tr>
                        <th class="text-center">Angle (º)</th>
                        <th class="text-center">Radians</th>
                        <th class="text-center">Sinus</th>
                        <th class="text-center">Cosinus</th>
                    </tr>
                </thead>
                <tbody>
                    <?php include 'act2.php';?>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>
