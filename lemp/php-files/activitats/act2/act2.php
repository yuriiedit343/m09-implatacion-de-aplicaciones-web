<?php
        // Iteració de 0 a 360 en increments de 1
        for ($angle = 0; $angle <= 360; $angle++) {
            $radians = deg2rad($angle);
            $sin = sin($radians);
            $cos = cos($radians);
            $deg2rad = $angle * (3.14 / 180);
            // Mostra els resultats en una fila de la taula
            echo "<tr>";
            echo "<td>$angle</td>";
            echo "<td>" . round($deg2rad,3) . "</td>";
            // Si el resultat és negatiu, afegim la classe CSS "negative"
            if ($sin < 0) {
                echo "<td class='negative'>" . number_format($sin, 4) . "</td>";
            } else {
                echo "<td>" . number_format($sin, 4) . "</td>";
            }
            if ($cos < 0) {
                echo "<td class='negative'>" . number_format($cos, 4) . "</td>";
            } else {
                echo "<td>" . number_format($cos, 4) . "</td>";
            }
            echo "</tr>";
        }
?>