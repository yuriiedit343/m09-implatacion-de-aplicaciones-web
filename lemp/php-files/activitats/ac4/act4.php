<?php


// Declaración de la longitud de la array
$len_array = 20;

// Creación de la array con valores aleatorios entre 100 y 999
$random_array = [];
for ($i = 0; $i < $len_array; $i++) {
    $random_array[] = rand(100, 999);
}

// Inicio de la primera tabla
echo "<h1>Taula Original</h1>";
echo "<table  class='table w-auto' border='1'>";

// Mostrar los índices en la primera fila
echo "<tr>";
foreach ($random_array as $index => $num) {
    echo "<td>" . ($index + 1) . "</td>";
}
echo "</tr>";

// Nueva fila para los valores de la array
echo "<tr>";

// Mostrar los valores de la array en la segunda fila
foreach ($random_array as $num) {
    echo "<td>" . $num . "</td>";
}
echo "</tr>";

// Fin de la primera tabla
echo "</table>";
echo "<br>";
echo "<br>";
echo "<br>";

echo "<h1>Taula Imparells/Parells</h1>";
// Inicio de la segunda tabla
echo "<table class='table w-auto' border='1'>";

// Mostrar los índices en la primera fila
echo "<tr>";
foreach ($random_array as $index => $num) {
    echo "<td>" . ($index + 1) . "</td>";
}
echo "</tr>";

// Nueva fila para los valores de la array
echo "<tr>";

// Ordenar los números entre pares e impares
$pares = [];
$impares = [];
$sumapares = 0;
$sumaimpares = 0;
foreach ($random_array as $num) {
    if ($num % 2 == 0) {
        $pares[] = $num;
        $sumapares=$sumapares + $num;
    } else {
        $impares[] = $num;
        $sumaimpares=$sumaimpares + $num;
    }
}
$sumatotal=$sumapares + $sumaimpares;
// Ordenar los números pares e impares
sort($pares);
sort($impares);

// Combinar pares e impares
$ordenados = array_merge($pares, $impares);

// Mostrar los valores de la array ordenados en la segunda fila
foreach ($ordenados as $num) {
    echo "<td>" . $num . "</td>";
}
echo "</tr>";

// Fin de la segunda tabla
echo "</table>";

echo "<p>Total Parells</p>";
echo "<p>$sumapares</p>";
echo "<p>Total Imparells</p>";
echo "<p>$sumaimpares</p>";
echo "<p>Total</p>";
echo "<p>$sumatotal</p>";

echo "<h1> Mitjana </h1>";
echo "<p>" . round($sumatotal / $len_array , 2) . "</p>";
echo "<br>";
echo "<br>";
echo "<br>";

echo "<h1>Taula Mutliples de 10</h1>";
// Inicio de la tercera tabla
echo "<table  class='table w-auto' border='1'>";



// Nueva fila para los valores de la array
echo "<tr>";

$multiples = [];

foreach ($random_array as $num) {
    if ($num % 10 == 0) {
        $multiples[] = $num;
    } 
}
$index=1;
foreach ($multiples as $num) {
    echo "<td>" . ($index) . "</td>";
    $index=$index+1;
}
echo "</tr>";
echo "<tr>";
// Mostrar los valores de la array multiples de 10
foreach ($multiples as $num) {
    echo "<td>" . $num . "</td>";
}
echo "</tr>";

// Fin de la segunda tabla
echo "</table>";


echo "<br>";
echo "<br>";
echo "<br>";

echo "<h1>Taula Mes petita que la mitjana</h1>";
// Inicio de la tercera tabla
echo "<table   class='table w-auto' border='1'>";

echo "<tr>";





// Nueva fila para los valores de la array
echo "<tr>";

$mespetit = [];

foreach ($random_array as $num) {
    if ($num < round($sumatotal / $len_array , 2)) {
        $mespetit[] = $num;
    } 
}
$index=1;
foreach ($mespetit as $num) {
    echo "<td>" . ($index) . "</td>";
    $index=$index+1;
}
echo "</tr>";
echo "<tr>";
// Mostrar los valores de la array multiples de 10
foreach ($mespetit as $num) {
    echo "<td>" . $num . "</td>";
}
echo "</tr>";

// Fin de la segunda tabla
echo "</table>";
echo "<br>";
echo "<br>";
echo "<br>";

echo "<h1>Taula Mes gran que la mitjana</h1>";
// Inicio de la tercera tabla
echo "<table   class='table w-auto' border='1'>";

// Mostrar los índices en la primera fila
echo "<tr>";

echo "</tr>";

// Nueva fila para los valores de la array
echo "<tr>";

$mesgran = [];

foreach ($random_array as $num) {
    if ($num > round($sumatotal / $len_array , 2)) {
        $mesgran[] = $num;
    } 
}


$index=1;
foreach ($mesgran as $num) {
    echo "<td>" . ($index) . "</td>";
    $index=$index+1;
}
echo "</tr>";
echo "<tr>";
foreach ($mesgran as $num) {
    echo "<td>" . $num . "</td>";
}

echo "</tr>";
// Fin de la segunda tabla
echo "</table>";
?>


