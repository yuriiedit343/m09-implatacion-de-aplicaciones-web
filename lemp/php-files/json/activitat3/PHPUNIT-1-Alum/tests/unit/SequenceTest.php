<?php 
declare(strict_types=1);

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

final class SequenceTest extends TestCase {

 
  #[Test]
  #[TestDox("Tribonacci")]
  public function testSequence() {

    require_once __DIR__ . "/../../src/php/Sequence.php";

    $seq = new Sequence();


    $this->assertSame([1,1,1,2,3,4,6,9,13,19],  $seq->sequence([1,1,1],10));
    $this->assertSame([0,0,1,1,1,2,3,4,6,9], $seq->sequence([0,0,1],10));
    $this->assertSame([0,1,1,1,2,3,4,6,9,13], $seq->sequence([0,1,1],10));
    $this->assertSame([1,0,0,1,1,1,2,3,4,6], $seq->sequence([1,0,0],10));
    $this->assertSame([0,0,0,0,0,0,0,0,0,0], $seq->sequence([0,0,0],10));
    $this->assertSame([1,2,3,4,6,9,13,19,28,41], $seq->sequence([1,2,3],10));
    $this->assertSame([3,2,1,4,6,7,11,17,24,35], $seq->sequence([3,2,1],10));
    $this->assertSame([1], $seq->sequence([1,1,1],1));
    $this->assertSame([], $seq->sequence([300,200,100],0));
    $this->assertSame([], $seq->sequence([1,2,3], 0));
    $this->assertSame([0.5,0.5,0.5], $seq->sequence([0.5,0.5,0.5],3));
    
}


}
    
