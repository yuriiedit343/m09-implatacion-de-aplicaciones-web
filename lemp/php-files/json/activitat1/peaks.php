<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista de Picos</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <?php
    // URL del fitxer JSON
    $url = "https://api.jsonserve.com/Gk1Go7";

    // Obté el contingut del fitxer JSON
    $json = file_get_contents($url);

    // Decodifica el JSON en un array associatiu
    $data = json_decode($json, true);

    // Comprova si s'ha pogut obtenir correctament les dades
    if ($data !== null) {
        // Itera sobre cada element de l'array
        foreach ($data as $peak) {
            echo "<div class='card'>";
            echo "<div class='card-body'>";
            echo "<h5 class='card-title'>" . $peak['name'] . "</h5>";
            echo "<img class='float-right' src='" . $peak['url'] . "' alt='" . $peak['name'] . "' style='width: 20%;'>";
            echo "<p class='card-text'>Alçada: " . $peak['height'] . " metres</p>";
            echo "<p class='card-text'>Prominència: " . $peak['prominence'] . " metres</p>";
            echo "<p class='card-text'>Zona: " . $peak['zone'] . "</p>";
            echo "<p class='card-text'>País: " . $peak['country'] . "</p>";
            
            echo "</div>";
            echo "</div>";
            echo "<br>";
        }
    } else {
        echo "No s'han pogut obtenir les dades JSON.";
    }
    ?>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
