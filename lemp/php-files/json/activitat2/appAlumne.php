<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>API SPORTS</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">

<?php

// Get all countries JSON file
$url_countries = "https://www.thesportsdb.com/api/v1/json/3/all_countries.php"; 
$json_countries = file_get_contents($url_countries);
$data_countries = json_decode($json_countries, true);
echo "<h1 style=text-align:center;>Select Country:</h1>";
// Get array "countries" from JSON object
$countries = $data_countries['countries'];
echo "<div style='display: flex; justify-content: center;'>";
// Create form
echo "<form method='POST' action='" . htmlspecialchars($_SERVER['PHP_SELF']) . "'>";

// Initialize selected country
$selected_country = "";

// Check if a country is selected
if (isset($_POST['country'])) {
    $selected_country = $_POST['country'];
}

// Add select field
echo "<select name='country' style='width:100%'>";

// Iterating $countries
foreach ($countries as $country) {
    // Check if the current option matches the selected country
    $selected = ($selected_country == $country['name_en']) ? "selected" : "";
    
    // Add all elements of array to <option>, adding the $selected variable
    echo "<option value='" . $country['name_en'] . "' " . $selected . ">" . $country['name_en'] . "</option>";
}

// Close select field
echo "</select>";

// Add submit button
echo "<br>";
echo "<div style=text-align:center>";
echo "<br>";
echo "<input type='submit' name='execute' class='btn btn-primary' value='Submit'>";
echo "</div>";
echo "<br>";
echo "<br>";
// Close form
echo "</form>";
echo "</div>";
// Check if form submitted
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['execute'])) {
    // Call execute($_POST['country']) method
    execute($selected_country);
}

// Define execute function
function execute($selected_country)
{
    // Here you can add your logic to execute based on the selected country
    $url_leagues = "https://www.thesportsdb.com/api/v1/json/3/search_all_leagues.php?c=" . $selected_country;
    $json_leagues = file_get_contents($url_leagues);
    $data = json_decode($json_leagues, true);
    
    if (isset($data['countries'])) {
        $leagues = $data['countries'];
        
        echo "<div class='table-responsive'>";
        echo "<table class='table table-bordered table-striped'>";
        echo "<thead class='thead-dark'>";
        echo "<tr>";
        echo "<th scope='col'>League</th>";
        echo "<th scope='col'>Badge</th>";
        echo "<th scope='col'>Website</th>";
        echo "<th scope='col'>Fanart</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        
        foreach ($leagues as $league) {
            echo "<tr>";
            echo "<td>" . $league['strLeague'] . "</td>";
            echo "<td><img src='" . $league['strBadge'] . "' alt='" . $league['strBadge'] . "' style='max-width: 150px;'></td>";
            echo "<td><a href='https://" . $league['strWebsite'] . "'>" . $league['strWebsite'] . "</a></td>";
            echo "<td><a href='" . $league['strFanart1'] . "' target='_blank'><img src='" . $league['strFanart1'] . "' alt='" . $league['strFanart2'] . "' style='max-width: 200px;'></a></td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td colspan='4'>" . $league['strDescriptionEN'] . "</td>";
            echo "</tr>";
        }
        
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "<p style='color: red;text-align:center'>No leagues found for this country.</p>";
    }
}

?>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
