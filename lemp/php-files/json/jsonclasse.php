<?php
//codificar :)
$age = array("Peter" => 35, "Ben" => 37, "Joe" => 43);
$json = json_encode($age);
var_dump($json);



echo "<br>>";
// decodificar si o si associatiu
$jsonobj = '{"Peter":35,"Ben":37,"Joe":43}';

var_dump(json_decode($jsonobj));

$obj = json_decode($jsonobj);
//accessing to objects
echo $obj->Peter . "<br>";
echo $obj->Ben . "<br>";
echo $obj->Joe . "<br>";
echo "<br>";

//Iterating $obj
$obj = json_decode($jsonobj);
foreach ($obj as $key => $value) {
    echo "[" . $key . ", " . $value . "]" . "<br>";
}
echo "<h2>URL JSON</h2>";
$url="https://jsonplaceholder.typicode.com/posts/1";
$data= file_get_contents($url);
$obj2=json_decode($data);

foreach ($obj2 as $key => $value) {
    echo "[" . $key . " => " . $value . "]" . "<br>";
}
?>