<?php
//procedural
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "bookstore";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "UPDATE book SET title = ? WHERE book_id = ?";
$stmt = $conn->prepare($sql);
$stmt->bind_param("si", $title, $book_id);

$book_id = 11128;
$title = "New Book2";
$num_pages = 1000; 

$stmt->execute();

//$result = $stmt->get_result(); // get the mysqli result

if($stmt->affected_rows > 0) {
    echo "Num rows updated: " . $stmt->affected_rows;
 } else {
    echo "Error when updating data";
 }


$stmt->close();
$conn->close();
?>