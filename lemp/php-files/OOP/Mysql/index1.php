<?php
$servername = "172.29.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";
//$port = 8085;

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} else {
    echo "Successfully connected" . "<br>";
}


//Selects 

$sql = "SELECT book_id,title,isbn13,num_pages FROM book ORDER BY title";
$result = $conn->query($sql);
// Después de la conexión exitosa
if ($result->num_rows > 0) {
    echo "<table border='1'>";
    echo "<tr><th>Book ID</th><th>Title</th><th>ISBN</th><th>Number of Pages</th></tr>";
    while ($row = $result->fetch_assoc()) {
        echo "<tr>";
        echo "<td>" . $row["book_id"] . "</td>";
        echo "<td>" . $row["title"] . "</td>";
        echo "<td>" . $row["isbn13"] . "</td>";
        echo "<td>" . $row["num_pages"] . "</td>";
        echo "</tr>";
    }
    echo "</table>";
} else {
    echo "No records found";
}
//Close connection
$conn->close();
?>