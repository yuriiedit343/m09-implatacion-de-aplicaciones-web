<?php
// Incluir las clases necesarias
require_once 'language.php';
require_once 'publisher.php';
require_once 'author.php';
require_once 'library.php';
require_once 'book.php'; // Asegúrate de incluir la clase Book después de las otras clases

$servername = "172.29.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

// Crear conexión
$conn = new mysqli($servername, $username, $password, $dbname);

// Verificar la conexión
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} else {
    echo "Successfully connected" . "<br>";
}

$search_book_id = "";
$title = "";
$isbn = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $search_term = isset($_POST['search_term']) ? $_POST['search_term'] : '';
     
    $sql = "SELECT b.book_id, b.title, b.isbn13, b.num_pages, b.publication_date,
                   bl.language_id, bl.language_code, bl.language_name,
                   p.publisher_id, p.publisher_name,
                   a.author_id, a.author_name
            FROM book b
            INNER JOIN book_language bl ON b.language_id = bl.language_id
            INNER JOIN publisher p ON b.publisher_id = p.publisher_id
            INNER JOIN book_author ba ON b.book_id = ba.book_id
            INNER JOIN author a ON ba.author_id = a.author_id
            WHERE LOWER(b.title) LIKE ?
            ORDER BY b.book_id";
    
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s", $search_query);
    $search_query = '%' . strtolower($search_term) . '%';
   
    $stmt->execute();
    $result = $stmt->get_result();

   
    // Crear un array para almacenar los objetos Book
    $books = array();

    if ($result->num_rows > 0) {
        // Iterar sobre los resultados y crear objetos Book
        while ($row = $result->fetch_assoc()) {
            // Obtener el idioma del libro
            $language = new Language($row["language_id"], $row["language_code"], $row["language_name"]);
            
            // Obtener el editor del libro
            $publisher = new Publisher($row["publisher_id"], $row["publisher_name"]);

            // Crear objeto Book y agregarlo al array
            $book = new Book($row["book_id"], $row["title"], $row["isbn13"], $language, $row["num_pages"], $row["publication_date"], $publisher);
            
            // Agregar autores al libro
            $author = new Author($row["author_id"], $row["author_name"]);
            $book->addAuthor($author);
            
            $books[] = $book;
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bookstore</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container mt-5">
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <div class="form-group">
            <label for="search_term">Search:</label>
            <input type="text" class="form-control" id="search_term" name="search_term" value="<?php echo $search_term; ?>">
        </div>
        <button type="submit" class="btn btn-primary">Search</button>
    </form>

    <?php if (isset($books) && count($books) > 0): ?>
        <table class="table mt-3">
            <thead>
                <tr>
                    <th>Book ID</th>
                    <th>Title</th>
                    <th>ISBN</th>
                    <th>Number of Pages</th>
                    <th>Publication Date</th>
                    <th>Authors</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($books as $book): ?>
                    <tr>
                        <td><?php echo $book->getBookId(); ?></td>
                        <td><?php echo $book->getTitle(); ?></td>
                        <td><?php echo $book->getIsbn13(); ?></td>
                        <td><?php echo $book->getNumPages(); ?></td>
                        <td><?php echo $book->getPublicationDate(); ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p>No records found</p>
    <?php endif; ?>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</body>
</html>

<?php
// Cerrar conexión
if (isset($conn)) {
    $conn->close();
}
?>
