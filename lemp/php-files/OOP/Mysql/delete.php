<?php
//procedural
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "bookstore";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "DELETE FROM book WHERE book_id = ?";
$stmt = $conn->prepare($sql);
$stmt->bind_param("i", $book_id);

$book_id = 11128;


$stmt->execute();

//$result = $stmt->get_result(); // get the mysqli result

if($stmt->affected_rows > 0) {
    echo "Num rows deleted: " . $stmt->affected_rows;
 } else {
    echo "Error when deleting data";
 }


$stmt->close();
$conn->close();
?>