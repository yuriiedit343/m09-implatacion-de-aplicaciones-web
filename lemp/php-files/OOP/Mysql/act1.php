<?php
$servername = "172.29.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} else {
    echo "Successfully connected" . "<br>";
}

$search_book_id = "";
$title = "";
$isbn = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(isset($_POST['search_book_id']) && !empty($_POST['search_book_id'])) {
        $search_book_id = $_POST['search_book_id'];
        $sql = "SELECT book_id, title, isbn13, num_pages, publication_date FROM book WHERE book_id LIKE '%$search_book_id%' ORDER BY book_id";
    } elseif (isset($_POST['title']) && !empty($_POST['title'])) {
        $title = $_POST['title'];
        $sql = "SELECT book_id, title, isbn13, num_pages, publication_date FROM book WHERE title LIKE '%$title%' ORDER BY title";
    } elseif (isset($_POST['isbn']) && !empty($_POST['isbn'])) {
        $isbn = $_POST['isbn'];
        $sql = "SELECT book_id, title, isbn13, num_pages, publication_date FROM book WHERE isbn13 LIKE '%$isbn%' ORDER BY isbn13";
    } else {
        $sql = "SELECT book_id, title, isbn13, num_pages, publication_date FROM book ORDER BY title";
    }

    $result = $conn->query($sql);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bookstore</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container mt-5">
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <div class="form-group">
            <label for="search_book_id">Search by Book ID:</label>
            <input type="number" class="form-control" id="search_book_id" name="search_book_id" value="<?php echo $search_book_id; ?>">
            <label for="title">Search by Title:</label>
            <input type="text" class="form-control" id="title" name="title" value="<?php echo $title; ?>">
            <label for="isbn">Search by ISBN:</label>
            <input type="text" class="form-control" id="isbn" name="isbn" value="<?php echo $isbn; ?>">
        </div>
        <button type="submit" class="btn btn-primary">Search</button>
    </form>

    <?php if (isset($result) && $result->num_rows > 0): ?>
        <table class="table mt-3">
            <thead>
                <tr>
                    <th>Book ID</th>
                    <th>Title</th>
                    <th>ISBN</th>
                    <th>Number of Pages</th>
                    <th>Publication Date</th>
                </tr>
            </thead>
            <tbody>
                <?php while ($row = $result->fetch_assoc()): ?>
                    <tr>
                        <td><?php echo $row["book_id"]; ?></td>
                        <td><?php echo $row["title"]; ?></td>
                        <td><?php echo $row["isbn13"]; ?></td>
                        <td><?php echo $row["num_pages"]; ?></td>
                        <td><?php echo $row["publication_date"]; ?></td>
                    </tr>
                <?php endwhile; ?>
            </tbody>
        </table>
    <?php elseif (isset($result)): ?>
        <p>No records found</p>
    <?php endif; ?>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script>
    $(document).ready(function(){
        $('#search_book_id').on('input', function() {
            if ($(this).val() !== '') {
                $('#title, #isbn').prop('disabled', true);
            } else {
                $('#title, #isbn').prop('disabled', false);
            }
        });

        $('#title').on('input', function() {
            if ($(this).val() !== '') {
                $('#search_book_id, #isbn').prop('disabled', true);
            } else {
                $('#search_book_id, #isbn').prop('disabled', false);
            }
        });

        $('#isbn').on('input', function() {
            if ($(this).val() !== '') {
                $('#search_book_id, #title').prop('disabled', true);
            } else {
                $('#search_book_id, #title').prop('disabled', false);
            }
        });
    });
</script>

</body>
</html>

<?php
//Close connection
if (isset($conn)) {
    $conn->close();
}
?>
