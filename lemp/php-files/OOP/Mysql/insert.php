<?php
//procedural
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "bookstore";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO book (book_id, title, isbn13, language_id, num_pages, publication_date, publisher_id) VALUES (?, ?, ?, ?, ?, ?, ?)";
$stmt = $conn->prepare($sql);
$stmt->bind_param("issiisi", $book_id, $title, $isbn, $language_id, $num_pages, $publication_date, $publisher_id);

$book_id = 11128;
$title = "New Book";
$isbn = "11111"; 
$language_id = 1; 
$num_pages = 100; 
$publication_date = "2000-01-01"; 
$publisher_id = 1; 

$stmt->execute();

$result = $stmt->get_result(); // get the mysqli result

if($stmt->affected_rows > 0) {
    echo "Num rows inserted: " . $stmt->affected_rows;
 } else {
    echo "Error when inserting data";
 }

$stmt->close();
$conn->close();
?>