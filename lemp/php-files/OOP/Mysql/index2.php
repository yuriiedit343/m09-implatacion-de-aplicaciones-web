<?php
$servername = "172.29.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";
//$port = 8085;

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT book_id, title, isbn13, publication_date, num_pages FROM book WHERE book_id LIKE ? AND title LIKE ?";
$stmt = $conn->prepare($sql);
$stmt->bind_param("is", $id, $title);
$id = 629;
$title = "Homebody"; 
$stmt->execute();

$result = $stmt->get_result(); // get the mysqli result

if ($result->num_rows > 0) {
    // output data of each row
    echo "Num rows: " . $result->num_rows;
    while ($row = $result->fetch_assoc()) {
        echo "book_id: " . $row["book_id"] . " - title: " . $row["title"] . ", " . $row["isbn13"] . ", " . $row["publication_date"] . ", " . $row["num_pages"] . "<br>";
    }
} else {
    echo "0 results";
}

mysqli_close($conn);
?>