<?php
class Product {
    //Properties
    private int $id;
    private string $nom;
    private float $price;
    private array $colors;
    //Constructor
    function __construct(int $id, string $nom, array $colors, float $price)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->colors = $colors;
        $this->price = $price;
    }
    //Destructor (Optional)
    function __destruct(){
        //unset($this);
    }
    //Getter and setters
    function get_id(): int {
        return $this->id;
    }
    function set_id(int $id): void{
        $this->id = $id;
    }
    function get_nom(): string {
        return $this->nom;
    }
    function set_nom(string $nom): void{
        $this->nom = $nom;
    }
    function get_price(): float {
        return $this->price;
    }
    function set_price(float $price): void{
        $this->price = $price;
    }
    function get_colors(): array {
        return $this->colors;
    }
    function set_colors(array $colors): void{
        $this->colors = $colors;
    }
    //Class Methods (optional)
    function tax(float $tax): float {
        return $tax / (1+$tax);
    }

    function notax(float $tax): float {
        return $this -> price /(1+$tax);
    }
    //toString  (optional)
    public function __toString(): string {
        return "Product[id=" . $this->id . ", name=" . $this->nom . ", price=" . $this->price . "]";
    }
}

$colors = ["Red", "Yellow", "Blue"];
$p1 = new Product(1, "Basic T-Shirt", $colors, 12.55);
$p2 = new Product(2, "Long T-Shirt", ["Black"], 18.25);

echo $p1->get_id() . "<br/>";
echo $p1->get_nom() . "<br/>";
echo $p1->get_price() . "<br/>";
$array_colors = $p1->get_colors();
print_r($array_colors);
var_dump($array_colors);

foreach ($colors as $color){
    echo $color . " ";
}

$products = [];
$products[] = $p1;
$products[] = $p2;

foreach ($products as $p) {
    echo $p->get_id() . " ";
    $colors = $p->get_colors();
    print_r($colors);
    echo "<br/>";
}

?>

<?php

echo "Methods";

echo $p1->notax(0.21);
echo "<br>";
echo $p1->tax(0.21);
echo "<br>";
echo "<br>";
echo "<br>";
echo $p2->__toString();