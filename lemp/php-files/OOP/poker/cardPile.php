<?php

require_once 'card.php';
class CardPile
{
    private array $pile;

    // Constructor
    public function __construct()
    {
        $this->pile = array();
    }

    // Methods
    // Function to add a card to the pile
    public function addCard(Card $card)
    {
        $this->pile[] = $card;
    }

    // Function to take a card from the pile
    public function removeCard(int $index): Card
    {
        $removedCard = $this->pile[$index];
        array_splice($this->pile, $index, 1);
        return $removedCard;
    }

    public function getNumCards(): int
    {
        return count($this->pile);
    }
}
?>

