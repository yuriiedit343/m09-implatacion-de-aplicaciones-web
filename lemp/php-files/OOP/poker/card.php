

<?php
class Card
{
    // Properties
    private int $value;
    private int $suit;
    private string $image;

    // Constructor
    public function __construct(int $value, int $suit)
    {
        $this->value = $value;
        $this->suit = $suit;
        $this->image = "cards/card" . (($suit - 1) * 13 + $value) . ".gif";


    }

    // Getters and setters
    public function getValue(): int
    {
        return $this->value;
    }

    public function getSuit(): int
    {
        return $this->suit;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    //toString
    public function __toString()
    {
        return "{$this->value} of {$this->suit}";
    }
}
?>
