<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>POKER</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            padding-top: 50px;
            background-color: #f8f9fa;
        }
        .card-container {
            display: flex;
            justify-content: center;
            flex-wrap: wrap;
            margin-top: 20px;
        }
        .card {
            margin: 5px;
        }
        .real-card {
    opacity: 0;
}

.rotating-card {
    animation: rotate 1.5s ease-out forwards; /* Duración de la animación */
}

@keyframes rotate {
    0% { transform: rotateY(180deg); opacity: 0; }
    100% { transform: rotateY(0deg); opacity: 1; }
}

.card-row {
    white-space: nowrap; /* Evita que las cartas se desplacen automáticamente a la siguiente línea */
    overflow-x: auto; /* Agrega una barra de desplazamiento horizontal si el contenido excede el ancho */
    margin-top: 20px; /* Espacio superior entre filas */
}

.card-container {
    display: inline-block; /* Muestra las cartas en línea */
    margin: 0 10px; /* Espacio horizontal entre cartas */
}



    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>POKER</h1>
                <form  method="GET">
                    <button type="submit" class="btn btn-primary">Play</button>
                </form>
            </div>
        </div>

        <?php
        include_once("app-alumne.php");
        // Incluimos el código para ejecutar el juego si se ha enviado el formulario
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            
            $cardPile = generateCards();
            $hand = getCards($cardPile);
            echo "<div class='row'>";
            echo "<div class='col-md-12 text-center'>";
            echo "<h2>Your Hand:</h2>";
            echo "<div class='card-container'>";
            showCards($hand);
            echo "</div>";
            echo "<br>";
            $text = getPoints($hand);
            
            showPoints($text);
            
            $total = getTotal($hand);
            echo "<br>";
            showTotal($total);
            echo "<br>";
            evaluate($hand);
            echo "</div>";
            echo "</div>";
        }
        ?>
    </div>
</body>
<script>

</script>
</html>