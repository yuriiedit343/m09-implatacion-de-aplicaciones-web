<?php
require_once 'card.php';
class Hand
{
    // Properties
    private array $cards;

    // Constructor
    public function __construct()
    {
        $this->cards = array();
    }

    // Getters and setters
    public function addCard(Card $card)
    {
        $this->cards[] = $card;
    }

    public function getCards(): array
    {
        return $this->cards;
    }
    public function setCards($cards): void
    {
        $this->cards=$cards;
    }

    //toString
    public function __toString()
    {
        $handString = "";
        foreach ($this->cards as $card) {
            $handString .= $card->__toString() . " ";
        }
        return trim($handString);
    }
}
?>
