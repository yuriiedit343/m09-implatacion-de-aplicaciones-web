<?php
require_once 'card.php';

class CardPile
{
    private array $cards;

    public function __construct()
    {
        $this->cards = array();
    }

    public function addCard(Card $card)
    {
        $this->cards[] = $card;
    }

    public function removeCard(Card $card): Card
    {
        $index = array_search($card, $this->cards);
        if ($index !== false) {
            $removedCard = array_splice($this->cards, $index, 1);
            return $removedCard[0];
        }
        return new Card(0,0,"");
    }

    public function getPile(): array
    {
        return $this->cards;
    }
}
?>
