<?php
include_once("card.php");
include_once("hand.php");
include_once("cardPile.php");

$hand = new Hand();

if ($_POST && isset($_POST['giveCards'])) {
    $cardPile = generateCards();
    $hand = getCards($cardPile);
    
    showCards($hand);
    
    $text = getPoints($hand);
    showPoints($text);
    
    $total = getTotal($hand);
    showTotal($total);
    
    evaluate($hand);
}

function generateCards(): CardPile
{
    $cardPile = new CardPile();
    for ($i = 1; $i <= 4; $i++) { // Bucle para cada palo
        for ($j = 1; $j <= 13; $j++) { // Bucle para cada valor
            $card = new Card($j, $i);
            $cardPile->addCard($card);
        }
    }
    return $cardPile;
}


function getCards(CardPile &$cardPile): Hand
{
    $hand = new Hand();
    for ($i = 0; $i < 5; $i++) {
        $randomIndex = rand(0, $cardPile->getNumCards() - 1);
        $card = $cardPile->removeCard($randomIndex);
        $hand->addCard($card);
    }

    return $hand;
}

function showCards(&$hand)
{
    $i = 1;
    echo "<div class='card-row'>";
    foreach ($hand->getCards() as $card) {
        echo "<div class='card-container' id='card-container$i'>";
        echo "<img src='cards/card_55.gif' alt='Card' class='card rotating-card' style='opacity: 1; z-index: 2;' id='card$i' onclick='toggleCard($i)'>"; // La carta giratoria tiene z-index mayor para estar encima de la carta real
        echo "<img src='" . $card->getImage() . "' alt='Card' class='card real-card' style='opacity: 0; z-index: 1;' id='real-card$i'>"; // La carta real está detrás de la carta giratoria
        echo "</div>";
        $i++;
    }
    echo "</div>"; // Cierre de la fila de cartas
    echo "<script>
            function toggleCard(cardId) {
                var rotatingCard = document.getElementById('card' + cardId);
                var realCard = document.getElementById('real-card' + cardId);
                
                if (rotatingCard.style.opacity === '1') {
                    rotatingCard.style.opacity = '0';
                    realCard.style.opacity = '1';
                } else {
                    rotatingCard.style.opacity = '1';
                    realCard.style.opacity = '0';
                }
            }
        </script>";
}





function getPoints(&$hand): string
{
    $points = "";
    foreach ($hand->getCards() as $card) {
        $value = ($card->getValue() % 13);
        if ($value == 1) {
            $value = 14;
        } elseif ($value == 11) {
            $value = 11;
        } elseif ($value == 12) {
            $value = 12;
        } elseif ($value == 0) {
            $value = 13;
        }
        $points .= $value . " ";
    }
    return trim($points);
}

function showPoints($text)
{
    echo "CARD POINTS:  $text";
}

function getTotal(&$hand): int
{
    $total = 0;
    foreach ($hand->getCards() as $card) {
        $value = $card->getValue();
        // Ajustar el valor del as a 14 en lugar de 1
        $value = ($value == 1) ? 14 : $value;
        // Sumar el valor de la carta al total
        $total += $value;
    }
    return $total;
}

function showTotal($total)
{
    echo "TOTAL POINTS: " . $total;
}

function evaluate(&$hand)
{
    echo "<div>";
    if (isRoyalFlush($hand)) {
        echo "RESULT: ROYAL FLUSH";
    } elseif (isStraightFlush($hand)) {
        echo "RESULT: STRAIGHT FLUSH";
    } elseif (isRepoker($hand)) {
        echo "RESULT: REPOKER";
    } elseif (isPoker($hand)) {
        echo "RESULT: POKER";
    } elseif (isFullHouse($hand)) {
        echo "RESULT: FULL HOUSE";
    } elseif (isFlush($hand)) {
        echo "RESULT: FLUSH";
    } elseif (isStraight($hand)) {
        echo "RESULT: STRAIGHT";
    } elseif (isThreeOfAKind($hand)) {
        echo "RESULT: THREE OF A KIND";
    } elseif (isTwoPairs($hand)) {
        echo "RESULT: TWO PAIRS";
    } elseif (isOnePair($hand)) {
        echo "RESULT: ONE PAIR";
    } else {
        echo "RESULT: HIGH CARD";
    }
    echo "</div>";
}


function isRoyalFlush($hand): bool
{
    $values = array_map(function($card) { return $card->getValue(); }, $hand->getCards());
    $suits = array_map(function($card) { return $card->getSuit(); }, $hand->getCards());
    sort($values);
    
    if ($suits[0] == $suits[1] && $suits[1] == $suits[2] && $suits[2] == $suits[3] && $suits[3] == $suits[4]) {
        if ($values[0] == 1 && $values[1] == 10 && $values[2] == 11 && $values[3] == 12 && $values[4] == 13) {
            return true;
        }
    }
    return false;
}

function isStraightFlush($hand): bool
{
    $values = array_map(function($card) { return $card->getValue(); }, $hand->getCards());
    $suits = array_map(function($card) { return $card->getSuit(); }, $hand->getCards());
    sort($values);
    
    if ($suits[0] == $suits[1] && $suits[1] == $suits[2] && $suits[2] == $suits[3] && $suits[3] == $suits[4]) {
        if (($values[0] == $values[1] - 1) && ($values[1] == $values[2] - 1) && ($values[2] == $values[3] - 1) && ($values[3] == $values[4] - 1)) {
            return true;
        }
    }
    return false;
}

function isRepoker($hand): bool
{
    $values = array_map(function($card) { return $card->getValue(); }, $hand->getCards());
    sort($values);
    
    if ($values[0] == $values[1] && $values[1] == $values[2] && $values[2] == $values[3] && $values[3] == $values[4]) {
        return true;
    }
    return false;
}

function isPoker($hand): bool
{
    $values = array_map(function($card) { return $card->getValue(); }, $hand->getCards());
    sort($values);
    
    if (($values[0] == $values[1] && $values[1] == $values[2] && $values[2] == $values[3]) ||
        ($values[1] == $values[2] && $values[2] == $values[3] && $values[3] == $values[4])) {
        return true;
    }
    return false;
}

function isFullHouse($hand): bool
{
    $values = array_map(function($card) { return $card->getValue(); }, $hand->getCards());
    sort($values);
    
    if (($values[0] == $values[1] && $values[1] == $values[2] && $values[3] == $values[4]) ||
        ($values[0] == $values[1] && $values[2] == $values[3] && $values[3] == $values[4])) {
        return true;
    }
    return false;
}

function isFlush($hand): bool
{
    $suits = array_map(function($card) { return $card->getSuit(); }, $hand->getCards());
    
    if ($suits[0] == $suits[1] && $suits[1] == $suits[2] && $suits[2] == $suits[3] && $suits[3] == $suits[4]) {
        return true;
    }
    return false;
}

function isStraight($hand): bool
{
    $values = array_map(function($card) { return $card->getValue(); }, $hand->getCards());
    sort($values);
    
    if (($values[0] == 1 && $values[1] == 10 && $values[2] == 11 && $values[3] == 12 && $values[4] == 13) ||
        ($values[0] == $values[1] - 1 && $values[1] == $values[2] - 1 && $values[2] == $values[3] - 1 && $values[3] == $values[4] - 1)) {
        return true;
    }
    return false;
}

function isThreeOfAKind($hand): bool
{
    $values = array_map(function($card) { return $card->getValue(); }, $hand->getCards());
    sort($values);
    
    if (($values[0] == $values[1] && $values[1] == $values[2]) ||
        ($values[1] == $values[2] && $values[2] == $values[3]) ||
        ($values[2] == $values[3] && $values[3] == $values[4])) {
        return true;
    }
    return false;
}

function isTwoPairs($hand): bool
{
    $values = array_map(function($card) { return $card->getValue(); }, $hand->getCards());
    sort($values);
    
    if (($values[0] == $values[1] && $values[2] == $values[3]) ||
        ($values[0] == $values[1] && $values[3] == $values[4]) ||
        ($values[1] == $values[2] && $values[3] == $values[4])) {
        return true;
    }
    return false;
}

function isOnePair($hand): bool
{
    $values = array_map(function($card) { return $card->getValue(); }, $hand->getCards());
    sort($values);
    
    if (($values[0] == $values[1]) ||
        ($values[1] == $values[2])||
      ($values[2] == $values[3]) ||
      ($values[3] == $values[4])) {
    return true;
  }
  return false;
}

?>