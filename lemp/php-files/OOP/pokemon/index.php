<?php
include_once("card.php");
include_once("hand.php");
include_once("cardPile.php");
include_once("app-alumne.php");

$hand = new Hand();

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['giveCards'])) {
    $cardPile = generateCards();
    $hand = new Hand(); // Crear una nueva mano
    showCards($hand);
    showTotal($total);
    evaluate($hand);
    // Resto del código para mostrar las cartas y el resultado
}
