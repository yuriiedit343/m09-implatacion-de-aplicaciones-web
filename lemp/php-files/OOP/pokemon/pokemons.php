<?php
    class Pokemons {
    private array $pokemons = [];

    // Constructor
    public function __construct() {
        // Constructor vacío
    }

    // Getters y Setters
    public function getPokemons(): array {
        return $this->pokemons;
    }

    public function setPokemons(array $pokemons): void {
        $this->pokemons = $pokemons;
    }

    // Método __toString
    public function __toString(): string {
        $output = "Pokemons:\n";
        foreach ($this->pokemons as $pokemon) {
            $output .= $pokemon . "\n";
        }
        return $output;
    }

    // Métodos de la clase
    public function addPokemon(Pokemon $pokemon): void {
        $this->pokemons[] = $pokemon;
    }

    public function getGeneration(int $generation): array {
        return array_filter($this->pokemons, function($pokemon) use ($generation) {
            return $pokemon->getGeneration() == $generation;
        });
    }

    public function getPokemonByName(string $text): array {
        return array_filter($this->pokemons, function($pokemon) use ($text) {
            return stripos($pokemon->getName(), $text) !== false;
        });
    }

    public function getPokemonByType(string $text): array {
        return array_filter($this->pokemons, function($pokemon) use ($text) {
            return stripos($pokemon->getType1(), $text) !== false || stripos($pokemon->getType2(), $text) !== false;
        });
    }
    public function getPokemonByCode(int $num): array {
        return array_filter($this->pokemons, function($pokemon) use ($num) {
            return stripos($pokemon->getCode(), $num)!== false;
        });
    }
}
?>