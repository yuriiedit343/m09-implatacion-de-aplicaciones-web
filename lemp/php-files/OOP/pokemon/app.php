<?php

$json_url = 'http://joanseculi.com/json/pokemons.json';
$json_data = file_get_contents($json_url);
$pokemons_data = json_decode($json_data, true);

include_once("pokemons.php");
include_once("pokemon.php");

$pokemons = new Pokemons();

foreach ($pokemons_data["pokemons"] as $pokemon_data) {
    $pokemon = new Pokemon(
        $pokemon_data['Code'],
        $pokemon_data['Name'],
        $pokemon_data['Type1'],
        $pokemon_data['Type2'],
        $pokemon_data['HealthPoints'],
        $pokemon_data['Attack'],
        $pokemon_data['Defense'],
        $pokemon_data['SpecialAttack'],
        $pokemon_data['SpecialDefense'],
        $pokemon_data['Speed'],
        $pokemon_data['Generation'],
        $pokemon_data['Legendary'],
        $pokemon_data['Image']
    );
    
    $pokemons->addPokemon($pokemon);
}

?>