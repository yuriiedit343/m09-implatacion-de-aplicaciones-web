<?php



class Pokemon {
    private int $code;
    private string $name;
    private string $type1;
    private string $type2;
    private int $healthPoints;
    private int $attack;
    private int $defense;
    private int $specialAttack;
    private int $specialDefense;
    private int $speed;
    private int $generation;
    private bool $legendary;
    private string $image;
    private int $total;

    public function __construct($code, $name, $type1, $type2, $healthPoints, $attack, $defense, $specialAttack, $specialDefense, $speed, $generation, $legendary, $image) {
        $this->code = $code;
        $this->name = $name;
        $this->type1 = $type1;
        $this->type2 = $type2;
        $this->healthPoints = $healthPoints;
        $this->attack = $attack;
        $this->defense = $defense;
        $this->specialAttack = $specialAttack;
        $this->specialDefense = $specialDefense;
        $this->speed = $speed;
        $this->generation = $generation;
        $this->legendary = $legendary;
        $this->image = $image;
        $this->total = $this->total();
    }

    public function total(): int {
        return $this->healthPoints + $this->attack + $this->defense + $this->specialAttack + $this->specialDefense + $this->speed;
    }

    // Getters
    public function getCode(): int {
        return $this->code;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getType1(): string {
        return $this->type1;
    }

    public function getType2(): string {
        return $this->type2;
    }

    public function getHealthPoints(): int {
        return $this->healthPoints;
    }

    public function getAttack(): int {
        return $this->attack;
    }

    public function getDefense(): int {
        return $this->defense;
    }

    public function getSpecialAttack(): int {
        return $this->specialAttack;
    }

    public function getSpecialDefense(): int {
        return $this->specialDefense;
    }

    public function getSpeed(): int {
        return $this->speed;
    }

    public function getGeneration(): int {
        return $this->generation;
    }

    public function isLegendary(): bool {
        return $this->legendary;
    }

    public function getImage(): string {
        return $this->image;
    }

    // Setters 
    public function setCode(int $code):void {
        $this->code = $code;
    }
    public function setName(string $name):void {
        $this->name = $name;   
    }
    public function setType1(string $type1):void 
    {
     $this->type1 = $type1;
    }
    public function setType2(string $type2):void {
            $this->type2 = $type2;
    }
    public function sethealthPoints(int $healthPoints):void {
        $this->healthPoints = $healthPoints;
    }
    public function setattack(int $attack):void {
        $this->attack = $attack;
    }
    public function setdefense(int $defense):void {
        $this->defense = $defense;
    }
    public function setSpeed(int $speed):void {
        $this->speed = $speed;
    }
    public function setspecialAttack(int $specialAttack):void {
        $this->specialAttack = $specialAttack;
    }
    public function setspecialDefense(int $specialDefense):void {
        $this->specialDefense = $specialDefense;
    }
    public function setGeneration(int $generation):void {
        $this->generation = $generation;   
    }
    public function setlegendary(bool $legendary):void {
        $this->legendary = $legendary;
    }
    public function settotal(int $total):void {
        $this->total = $total;
    }

    public function __toString(): string {
        return "Pokemon: " . $this->name . " - Code: " . $this->code . " - Generation: " . $this->generation;
    }

}


?>