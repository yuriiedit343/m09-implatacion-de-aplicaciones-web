<!doctype html>
<html>
<head>
  <title>Examen Anime</title> 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script></head>
<body>
<?php
$url_anime = "https://joanseculi.com/edt69/animes3.php";
$json_anime = file_get_contents($url_anime);
$data_anime = json_decode($json_anime, true);
echo "<h1 style=text-align:center>Animes</h1>";
echo "<table border='1' style=margin:auto>";
echo "<td>Num animes:</td>";
$cont = 0;
foreach ($data_anime['animes'] as $anime) {
    $cont = $cont + 1;
}
echo "<td>{$cont}</td>";
echo "</table>";
echo "<br>";
echo "<table class='table table-bordered'>";

foreach ($data_anime['animes'] as $anime) {
    echo "<tr><th>Image</th><th>ID</th><th>Name</th><th>Type</th><th>Year</th><th>Genre</th><th>Rating</th><th>Demography</th><th>Originalname</th></tr>";
    echo "<tr>";
    echo "<td><img style=width:100% src='https://joanseculi.com/{$anime['image']}'></td>";
    echo "<td>{$anime['id']}</td>";
    echo "<td>{$anime['name']}</td>";
    echo "<td>{$anime['type']}</td>";
    echo "<td>{$anime['year']}</td>";
    echo "<td>{$anime['genre']}</td>";
    
    echo "<td>{$anime['rating']}</td>";
    echo "<td>{$anime['demography']}</td>";
    echo "<td>{$anime['originalname']}</td>";
    echo "<tr colspan=9><th colspan=9>Synopsis</th></tr>";
    echo "<tr>";
    echo "<td colspan=9>{$anime['description']}</td>";
    echo "</tr>";
    echo "</tr>";
}
echo "<div class='container mt-3'>";
$genres = [];
foreach ($data_anime['animes'] as $anime) {
    $genre_list = explode(", ", $anime['genre']);
    $genres = array_merge($genres, $genre_list);
}
$genres = array_unique($genres);
sort($genres);

echo "<table class='table table-bordered' style=width:50%>";
echo "<td>Tots els gèneres:</td>";

foreach ($genres as $genre) {
    echo "<td>$genre</td>";
}

echo "</table>";

$years = [];
foreach ($data_anime['animes'] as $anime) {
    $years[] = $anime['year'];
}
$years = array_unique($years);
sort($years);

echo "<br>";

echo "<table class='table table-bordered'>";
echo "<td>Tots els anys:</td>";

foreach ($years as $year) {
    echo "<td>$year</td>";
}

echo "</table>";
echo "</div>";


?>
</body>
</html>